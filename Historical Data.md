# Historical Data

## Archives centers

### Archives of the Bank of France. 
- Link: https://www.banque-france.fr/la-banque-de-france/histoire/archives-historiques.html 
- Description: The Archives of the Bank of France allow several historiographical and economic perspectives. It will interest researchers in monetary history, economic history, international relations, and political science. It also allows to write the internal history of the Bank.

### Archives of the Bank of International Settlements. 
- Link:  http://www.bis.org/about/archive.html
- Description: The records are paper-based, with a selection of records relating to the Bank's activities during the period 1930-48 also available as scanned versions. In addition, the BIS Archive possesses a small photo collection.
The records of the BIS are organised in seven record groups, largely reflecting the BIS's organisational structure:
Organisation and Management of the BIS
Banking
Monetary and Economic Department
Accounting and loans
Establishment - building, security, HR policies, IT systems etc (This record group is not open to the public)
Finance and economics
History
The record groups consist largely of various types of correspondence, internal communication and reports. Important collections include files on:
Reparations Office: secretariat files and a documentation library originating from the Berlin Reparations Office (Agent General for Reparations Parker Gilbert). These files were taken over by the BIS upon the dissolution of the Reparations Office in 1930. They document the administration of reparations during the 1920s, a function taken over by the BIS and performed until the suspension of reparations by the Lausanne Agreement (1932).
The administration of international loans issued in connection with the Dawes and Young Plans (Dawes and Young Loans).
The operation of the European Payments Union and the European Monetary Agreement from the late 1940s onward.
International credit facilities in the granting of which the BIS has been involved, eg 1966 and 1968 Sterling Group Arrangements.
Expert meetings and conferences organised by the BIS or in which the BIS participated.
Papers of senior managers: eg R Auboin, Dr E Hülse and Dr P Hechler. Papers of the first Presidents: G McGarrah and L Fraser. Additionally, we have microfilms of a selection of T H McKittrick's papers from the Baker Library, Harvard University.
There are also over 250 customer and counterparty files, documenting banking policy and banking transactions between the BIS and its counterparties from 1930 (mainly central banks and international institutions).
Detailed information on the BIS archive collections that have been released for consultation by external researchers can be found in the BIS Archive Guide.

### Archives of the Chamber of Commerce of Paris
- Link: https://archibibli.wordpress.com/2015/06/13/les-archives-de-la-chambre-de-commerce-et-dindustrie-de-paris-autour-de-fonds-specialises/
- Description: Les archives de la Chambre de commerce et d’industrie de Paris sont constituées de fonds présentant la correspondance générale, des dossiers de relations extérieures et les dossiers préparatoires aux avis de la chambre de 1803 à 1986.
Les séries thématiques sont : Représentation commerciale et professionnelle (I), Organisation commerciale et industrielle (II), Législation (III), Transports, voies et moyens de communication (IV), Questions politiques, militaires et sociales (V), Production et commerce (VI).
Accès:
Direction de l’information Service central des archives 27, avenue de Friedland 75382 Paris Cedex 08
Métro: Charles de Gaulle – Etoile, George V
archives@ccip.fr
Horaires:
La consultation des archives historiques a lieu uniquement sur rendez-vous,
à prendre 48 h à l’avance.

### French National Archives
- Link: http://www.archives-nationales.culture.gouv.fr
- Description: The Archives nationales (French for "National Archives") preserve the national archives of France, apart from the archives of the Ministry of Defence and the Ministry of Foreign Affairs, as these two ministries have their own archive services, the Service historique de la défense and the Archives diplomatiques respectively. The Archives nationales have one of the largest and most important archival collections in the world, a testimony to the very ancient nature of the French state which has been in existence for more than twelve centuries already.
The Archives nationales were created at the time of the French Revolution in 1790, but it was a state decree of 1794 that made it mandatory to centralize all the pre-French Revolution private and public archives seized by the revolutionaries, completed by a law passed in 1796 which created departmental archives (archives départementales) in the départements of France to alleviate the burden on the Archives nationales in Paris, thus creating the collections of the Archives nationales as we know them today. In 1800 the Archives nationales became an autonomous body of the French state. Today, they contain about 406 km. (252miles) of documents (the total length of occupied shelves put next to each other), an enormous mass of documents growing every year. The original documents stored by the Archives nationales range from AD 625 to today.
The Archives nationales are under the authority of the French Archives Administration (Service interministériel des Archives de France) in the Ministry of Culture. The Archives of France also manage the 100 departmental archives located in the préfectures of each of the 100 départements of France, as well as various other local archives. These departmental and local archives contain all the archives from the decentralized branches of the French state, as well as all the archives of the pre-French Revolution provincial and local institutions seized by the revolutionaries (parlements, chartered cities, abbeys, churches, etc.). Thus, in addition to the 252 miles (406 km) of documents kept by the Archives nationales, at least 1,753 miles (2,821 km) of documents are kept in the departmental and local archives, in particular the church records and notarial records used by genealogists.

### Archives of the UN in Geneva
- Link: http://www.unog.ch/80256EE60057D930/(httpPages)/B2B526D168EEB91DC1257C850047CDCD?OpenDocument
- Description: The United Nations Office at Geneva (UNOG) Library, previously the League of Nations Library, was founded in 1919 and became the United Nations Library at Geneva when the League’s assets were transferred to the United Nations in 1946. (Further Information about the history of the Library and Archives)
Inventory: 
The goal is to provide a modern library suitable for the study of international relations. It collects books, periodicals and electronic resources to support the programmes and activities of the Organization: international law, international relations, political science, humanitarian affairs, human rights, disarmament, economic and social development, etc. 
The Library’s unique collections date back to the League of Nations, including some rare materials from the pre-League period. It is a complete depository for United Nations documents and publications and maintains a comprehensive collection of materials of the specialized agencies and the United Nations affiliated bodies.
The archives contain the documents of the League of Nations (1919-1946), but also about peace movements and international relations in general from the end of the nineteenth century. Part of the archival collection are also private papers of officials and delegates and iconographic holdings.

### Archives of the UN in New York
- Link: https://archives.un.org
- Description: The Archives and Records Management Section (ARMS) identifies, preserves and provides access to those records that document the history of the United Nations.
ARMS is responsible for all aspects of UN record-keeping, ranging from measures to ensure that United Nations officials create records in the course of their duties, through the management of records in United Nations offices, to preserving and making records of continuing value accessible as United Nations archives.

### Archives of the IMF
- Link: https://www.imf.org/external/np/arc/eng/archive.htm
- Description: Starting September 1, 2016 the Archives of the IMF will only be available electronically to external users. All requests to view records will be processed following the IMF polices on access. Those records that can be declassified will be made available electronically through the IMF Archives website. No site visits will be allowed after this date, unless an exception is granted for special circumstances. Documents from the Executive Board and selected Bretton Woods collections are available online using the IMF Archives Catalogue. Other publicly available materials are posted on www.imf.org.

### Archives of the French Finance Ministry
- Link: http://www.economie.gouv.fr/caef
- Description: Le Centre des archives économiques et financières (CAEF) détient des fonds et collections très divers pour chercheurs et amateurs, issus des services des ministères économiques et financiers ou des établissements publics et autorités administratives qui y sont rattachés.
Les archives historiques sont organisées autour de différents pôles : archives des ministres, des directions d'administration centrale et services industriels rattachés au ministère (Imprimerie nationale, SEITA, Monnaies et médailles, Service des alcools, Loterie nationale), archives de la Compagnie des agents de change de Paris. Ces archives sont complétées de fonds privés et d'une bibliothèque issue de celle du ministère des finances. Les collections regroupent des monographies concernant les principaux champs de compétences du ministère : économie, finances, budget, comptes publics, réforme de l’Etat, législation fiscale, industrie.

### Archives of the Bank of England
- Link: http://www.bankofengland.co.uk/archive/Pages/default.aspx
- Description: The Bank of England’s Archive contains over 80,000 ledgers, files and individual records relating to all aspects of the history of the Bank and its work, dating from its foundation in 1694 to the present. The Archive supports the work of the Bank today, and provides facilities for researchers from all over the world. The Bank's records are of prime importance to economic historians, but its holdings are of interest to social, local, and business historians, architectural specialists, biographers and genealogists. In addition to long series of customer account and stock ledgers, the Bank's records include branch records, architectural plans and drawings, staff records, diaries and papers of members of staff, records from the Bank's solicitors which include case files on forgery and prisoners' correspondence, and modern files detailing changing policies, its day-to-day work and relationships with other central banks and governments.

### Archives of the Bank of France
- Link: https://www.banque-france.fr/la-banque-de-france/histoire/archives-historiques.html
- Description: Les archives de la Banque de France permettent plusieurs approches historiographiques. Elles intéresseront les chercheurs en histoire monétaire, en histoire économique, quel que soit l’échelon géographique étudié (mondial, national ou régional), et ceux qui étudient les relations internationales. Toutes ces composantes renvoient à des interrogations et prises de décisions qui intéresseront évidemment la recherche en sciences politiques. Elles permettent également d’écrire l’histoire interne de la Banque de France tant d’un point de vue social, qu’industriel ou encore organisationnel. Enfin, conservés depuis la création de la Banque de France, les dossiers du personnel intéresseront les biographes et les généalogistes. Les archives historiques de la Banque de France sont constituées d’environ 70 000 articles couvrant une période allant de 1800 à nos jours. La totalité de ces archives a été versée par les directions et services à l’exception de celles du Comte d’Argout, Gouverneur entre 1834 et 1836, qui ont été achetées en 1993 et des archives privées du Sous-gouverneur Rist déposées par ses descendants en 2006. 

## Online Historical Data

### Clio.infra
- Link: https://www.clio-infra.eu
- Description: In 2010, the Netherlands Organisation for Scientific Research (NWO) awarded a subsidy to the Clio Infra project, of which Jan Luiten van Zanden was the main applicant and for which the International Institute of Social History (IISH) fulfills the secretarial function. Under the title of Clio Infra, a set of interconnected databases has been set up containing worldwide data on social, economic, and institutional indicators for the past five centuries, with special attention to the past 200 years. These indicators allow research into long-term development of worldwide economic growth and inequality.

### HFS
- Link: http://www.centerforfinancialstability.org/hfs.php
- Description: Historical Financial Statistics (HFS), a free, noncommercial data set on exchange rates, central bank and commercial bank balance sheets, interest rates, money supply, inflation, international trade, government finance, national accounts, and more. Our focus is data from roughly 1500 to 1950, although we have earlier and later data. Historical Financial Statistics currently contains about 150,000 annual data points and more than 2 million higher-frequency data points. It is intended to complement a number of long-established databases whose coverage begins in the mid 20th century. The editor of Historical Financial Statistics is Kurt Schuler, Senior Fellow in Financial History at the Center for Financial Stability (CFS). The data in Historical Financial Statistics are available thanks to the generosity of many researchers.

### NBER Macrohistory database

- Link: http://www.nber.org/databases/macrohistory/contents/
- Description: During the first several decades of its existence, the National Bureau of Economic Research (NBER) assembled an extensive data set that covers all aspects of the pre-WWI and interwar economies, including production, construction, employment, money, prices, asset market transactions, foreign trade, and government activity. Many series are highly disaggregated, and many exist at the monthly or quarterly frequency. The data set has some coverage of the United Kingdom, France and Germany, although it predominantly covers the United States. For information see Improving the Accessibility of the NBER's Historical Data , by Daniel Feenberg and Jeff Miron. (NBER Working Paper #5186). Published in the Journal of Business and Economic Statistics, Volume 15 Number 3 (July 1997) pages 293-299.

### Maddison Database
- Link: http://www.ggdc.net/maddison/maddison-project/data.htm
- Description: The Maddison Project has launched an updated version of the original Maddison dataset in January 2013. The update incorporates much of the latest research in the field, and presents new estimates of economic growth in the world economic between AD 1 and 2010. The new estimates are presented and discussed in Bolt and Van Zanden (2014). The Maddison Project: collaborative research on historical national accounts. The Economic History Review, 67 (3): 627–651.

### Ricardo
- Link: http://ricardo.medialab.sciences-po.fr/#/
- Description: RICardo (Research on International Commerce) is a database of bilateral trade flows of all the world’s countries that covers a period of more than a hundred years, from the beginning of the Industrial Revolution to the eve of World War II. The website aims to explore the history of international trade through trade exchanges between countries of the different continents.

#### Toflit18
- Link: http://toflit18.hypotheses.org/category/presentation-of-the-project
- Description: This project was granted funding by ANR in 2013 for the period 2014-2017. The project “Transformations of the French Economy through the Lens of International Trade, 1716-1821” (TOFLIT18) aims at improving our knowledge of the French economy during the period that laid the economic ground for the entry of France and Europe in the modern industrial era. Its main tools are the retranscription, the use and the diffusion of French international trade statistics.

### European State Finance Database
- Link: http://www.esfdb.org
- Description: The European State Finance Database represents the outcome of an international collaborative research project for the collection, archiving and dissemination of data on European fiscal history across the medieval, early modern and modern periods. This material is available free of charge to academics and students, in order to fulfil three main aims:
To conserve the data underlying previous published and unpublished research, in order that subsequent researchers do not have to re-collect data in order to verify older calculations; 
To disseminate this data and in doing so encourage new research;
To continue to preserve new data relating to state finance as it becomes available.
The data available is drawn from the main extant sources of a number of European countries, as the evidence and the existing state of scholarship permit. As contributions are made on a voluntary basis, the ESFD cannot guarantee to provide all data relating to all research conducted in the field of state finance. However it aims to provide as comprehensive coverage as possible. Where there are typographical errors in migrated datasets that also appear in the original published work, for example an incorrect date given in the title of a graph, these have been retained in the enhanced database. However the new graphs produced will have been corrected so that they reflect the correct data span. In the newly deposited datasets the codes are provided in full, whereas the migrated datasets contain abbreviated codes for which a key has then been provided. The newly deposited datasets provide figures in actual units rather than hundreds or thousands of units. Insofar as possible, we would be grateful if new contributors would adhere to these general principles.
The database is structured along the following lines, with each element being described in greater detail later in this introduction: 
Introduction: A brief introduction to each series of datasets;
Database: The list of all the datasets available, by series, and a search engine;
Bibliography: A bibliography of secondary sources;
New Data Submission: A form for depositing new data.
For each dataset, statistics derived from the primary sources are presented in tabular and graphical form, both of which can be downloaded. Note files outline the primary sources from which the dataset was derived. The ESFDB requires all users to give full and proper acknowledgement (including specific citation) in any resulting scholarly work or publication to the name of the contributing scholar whose files they have used, including the specific filenames. A specific standard citation detail is provided for each dataset.

### GGDC
- Link: http://www.rug.nl/ggdc/
- Description: The Groningen Growth and Development Centre (GGDC). The GGDC is a platform for research on economic growth and development. This research is largely based on a range of comprehensive databases on indicators of growth and development that the Centre compiles and maintains on a regular basis. Furthermore the GGDC organises seminars and conferences. This website is divided into three main research areas: Productivity, Value Chainsand Historical Development. Each area includes several databases, along with documentation, references to related research papers, as well as relevant news items and events.

### Online Archives of the League of Nations
- Link: http://libraryresources.unog.ch/c.php?g=462663&p=3163196
- Description: League of Nations Official Journal, official documents and serial publications, 1919-1946. A searchable index produced in connection with the microfilm project "League of Nations Documents, 1919-1946" (published by Research Publications, now Primary Source Microfilm, an imprint of the Gale Group).

### Archives of The Economist
- Link: https://ukshop.economist.com/products/the-economist-historical-archive?_ga=1.128749132.1322889237.1420157873
- Description: Subscribe for one year's access to 169 years' worth of articles. The archive contains more than 160 years of online content, dating from the first issue in 1843 until 2012. Each issue is as it appeared in print. 

### Mormons’ genealogical data
- Link: http://www.searchforancestors.com/mormongenealogy.html
- Description: In the past, the Mormon genealogy records consisted of extracted data and member submissions (the IGI, Ancestral File, and Pedigree Resource File), but the new FamilySearch site offers rich international collections of historical records that consist of vital records, census records, and military records. Many of the record collections have online images that can be downloaded for free. New databases are being added at a quick pace and are browseable until the indexes are completed. You may have to create and sign into a Family Search account, but it is always free.

### Macrohistory database of Bonn University
- Link: http://www.macrohistory.net/data/
- Description: The Jordà-Schularick-Taylor Macrohistory Database is the result of an extensive data collection effort over several years. In one place it brings together macroeconomic data that previously had been dispersed across a variety of sources. On this website we provide convenient no-cost open access under a license to the most extensive long-run macro-financial dataset to date. Commercial data providers are strictly forbidden to integrate all or parts of the dataset into their services or sell the data (see Terms of Use and Licence Terms below).
The database covers 17 advanced economies since 1870 on an annual basis. It comprises 25 real and nominal variables. Among these, there are time series that had been hitherto unavailable to researchers, among them financial variables such as bank credit to the non-financial private sector, mortgage lending and long-term house prices. The database captures the near-universe of advanced-country macroeconomic and asset price dynamics, covering on average over 90 percent of advanced-economy output and over 50 percent of world output.
Assembling the database, we relied on the input from colleagues, coauthors and doctoral students in many countries, and consulted a broad range of historical sources and various publications of statistical offices and central banks. For some countries we extended existing data series, for others we relied on recent data collection efforts by others. Yet in a non-negligible number of cases we had to go back to archival sources including documents from governments, central banks, and private banks. Typically, we combined information from various sources and spliced series to create long-run datasets spanning the entire 1870–2014 period for the first time.

### ILO Labour database
- Link: http://www.ilo.org/travail/areasofwork/wages-and-income/WCMS_142568/lang--es/index.htm
- Description; The ILO Global Wage Database covers four indicators from 1995 to 2013 for all ILO member States (where available): minimum wages, average nominal wages, average real wages and average real wage growth. In previous years, the data underlying the Global Wage Report included many additional indicators (e.g. low pay, wage inequality by decile, wages by sex, etc.). However, since the publication of the Global Wage Report 2012/13, the compilation of these indicators has been transferred to the “Yearly indicators” collection of ILOSTAT at: /ilostat ).

### Historical statistics.org
- Link: http://www.historicalstatistics.org
- Description: Historical currency converter. Historical monetary and financial statistics for Sweden.

### EH.net
- Link: http://eh.net/databases/
- Description: EH.Net provides an on-line location for researchers in economic history to make their data series available to other professionals and interested scholars. Several data series have been given to EH.Net and are available as downloadable files, while many other titles may be accessed through our Database Directory.
Data Series Online
The following data series are hosted by EH.Net:
Realized Rates of Return to U. K. Home and Overseas Investments
Biweekly Data on the Confederate Grayback Note Price of a Gold Dollar in Richmond and Houston
Developing Country Export Statistics: 1840, 1860, 1880 and 1900
Early Forward Exchange Markets: Vienna, 1876-1914
Early U.S. Securities Prices, 1790-1860
Global Financial Data, 1880-1913
Greenback Series
Historical Labor Statistics Project Series
U.S. Agricultural Workforce,1800-1900
U.S. Customs House Data, 1854-59
U.S. Government Bond Trading Database, 1776-1835
U.S. National Bank Notes, 1864-1935
U.S. Population Series
U.S. Public Debt Issues, 1775-1976
Unskilled wage index, U.S.
Weekly Data on Confederate Cotton Bond Prices in London and Junk Bond Prices in Amsterdam[.xls]
Wheat Prices in France, 1825-1913
Cole monthly commodity prices, 1700-1861
New England 1631-1776, sample of 18,509 probates
Bank of England Gilt-Edged Transactions 1951 – 1959
U.S. Censuses of Manufacturing: Samples from 1850, 1860, 1870 and 1880
International Currencies 1890-1910
Quinquennial U.S. special census of the electrical light and power industry (1902-1937)
Federal Reserve Bank of St Louis Historial Series and Digital Library
Caja Files: Royal Treasury Data for the Spanish Colonies
 
### Global price and income history group
- Link: http://gpih.ucdavis.edu/#
- Description: Prices, income, and well-being in the world before 1950.

### US Census
- Link: http://www.census.gov/population/international/data/
International Data Base (IDB)
Find demographic indicators, population pyramids, and source information for countries and areas of the world with a population of 5,000 or more.
World Population Summary
Find the latest information on the World Population trends.
Country Rankings
Find historical and up-to-date Country Rankings based on the population size.
HIV/AIDS Surveillance
Access data resources from various sources on HIV/AIDS prevalence and incidence for countries in Africa, Asia, Europe, Latin America and the Caribbean, and Oceania.
Global Population Mapping
The Global Mapping Team develops global, subnational population data and maps for other federal agencies and the public for use in international humanitarian relief and crisis planning.

### HSN
- Link: https://socialhistory.org/en/hsn/index
- Description: The HSN offers a representative sample of about 78.000 people born in the Netherlands during the period 1812-1922. The HSN-database containing individual life-courses is a unique tool for research in Dutch history and demography.

### IISH
- Link: http://www.iisg.nl/hpw/data.php
- Description: Data on prices and wages are among the most important sources of information in social- and economic-historical research, especially for the pre-statistical period. The International Institute of Social History (IISH) has taken the initiative to set up a network of scholars working with this kind of data and establish a moderated list of datafiles of historical prices and wages. Scholars working in this field are invited to register their work, and to make their data bases available through the internet - either on their own webpages (which may be made accessible via the IISH-list) or on the webpages of the IISH. The long term goal is to enhance the exchange and (re-)use of these data in order to write truly international-comparative histories of the development of markets and their institutional settings. The focus will be on data bases related to Europe and on non-European (in particular Asian) countries in the period before 1914. All data bases will not only include the data themselves, but also descriptions of the way in which they are constructed, the sources which are used, and relevant publications in which the data are analysed.

### IISH Indonesia
- Link: https://socialhistory.org/en/projects/indonesian-economic-development-1800-2000
- Description: This project is a concerted effort of several researchers at different universities and research institutions. The aim of the project is to reconstruct the national accounts of Java (1815-1939) and of Indonesia (1900-2000) and to analyze the long-term evolution of the economy of Indonesia in this period. The reconstructed national accounts will serve as the prime source of information about the economy's development over time. This provides an analytical framework for a more thorough understanding of changes and discontinuities in the economic performance of Indonesia.

### Historical statistics of Japan
- Link: http://www.stat.go.jp/english/data/chouki/index.htm
- Description: Important time-series data from a vast amount of statistics covering various fields including land, population, economy, society and culture and compiled them systematically into the "Historical Statistics of Japan." The statistics covered in this publication range from 1868 to date.

### Irish historical statistics
- Link: http://www.tcd.ie/iiis/HNAG/HNAG_database.htm
- Description: The Database is intended as a common resource for all scholars working in the field of Irish economic history. It is edited by Jason Begley (begleyj@tcd.ie), Frank Geary (F.Geary@ulst.ac.uk) and Kevin O'Rourke (kevin.orourke@tcd.ie). Scholars are welcome to use the data made available here. The database should be cited in any conference paper or publication arising. Frank Geary and Kevin O'Rourke wish to acknowledge a grant of IR£10,000 from the Royal Irish Academy which enabled the initial collection of series. Brendan Lynn was responsible for much of this early work. The Database has been established with the aid of TCD IS Services and the Institute for International Integration Studies. The editors welcome contributions from scholars who have data that they wish to make available to a wider public. Any scholar wishing to lodge data with the database should contact one of the editors. All contributions which are accepted for the database will be acknowledged.

### Human mortality database
- Link: http://www.mortality.org
- Description: The Human Mortality Database (HMD) was created to provide detailed mortality and population data to researchers, students, journalists, policy analysts, and others interested in the history of human longevity. The project began as an outgrowth of earlier projects in the Department of Demography at the University of California, Berkeley, USA, and at the Max Planck Institute for Demographic Research in Rostock, Germany (see history). It is the work of two teams of researchers in the USA and Germany (see research teams), with the help of financial backers and scientific collaborators from around the world (see acknowledgements). The Center on the Economics and Development of Aging (CEDA) French Institute for Demographic Studies (INED) has also supported the further development of the database in recent years.

### IISH prices and wages
- Link: http://www.iisg.nl/hpw/
- Description: Data on prices and wages are among the most important sources of information in social- and economic-historical research, especially for the pre-statistical period. The International Institute of Social History (IISH) has taken the initiative to set up a network of scholars working with this kind of data and establish a moderated list of datafiles of historical prices and wages. Scholars working in this field are invited to register their work, and to make their data bases available through the internet - either on their own webpages (which may be made accessible via the IISH-list) or on the webpages of the IISH. The long term goal is to enhance the exchange and (re-)use of these data in order to write truly international-comparative histories of the development of markets and their institutional settings. The focus will be on data bases related to Europe and on non-European (in particular Asian) countries in the period before 1914. All data bases will not only include the data themselves, but also descriptions of the way in which they are constructed, the sources which are used, and relevant publications in which the data are analysed.

### MEMBDB
- Link: http://www2.scc.rutgers.edu/memdb/aboutmemdb.html
- Description: The Medieval and Early Modern Data Bank is a project established at Rutgers University and originally cosponsored by the Research Libraries Group (RLG), Inc. Its aim is to provide scholars with an expanding library of information in electronic format on the medieval and early modern periods of European history, circa 800-1815 C.E.
MEMDB contains five large data sets, three pertaining to currency exchanges and two pertaining to prices:
Currency Exchanges (Metz) contains monetary data from Rainer Metz, Geld, Währung und Preisentwicklung: der Niederrheinraum im europäischen Vergleich, 1350-1800 (Frankfurt am Main, 1990). 
Currency Exchanges (Mueller) contains monetary data supporting material presented in Reinhold C. Mueller, The Venetian Money Market: Banks, Panics, and the Public Debt, 1200-1500 (Baltimore, 1997). 
Currency Exchanges (Spufford) contains all currency exchange quotations compiled by Peter Spufford and published in his Handbook of Medieval Exchange (London, 1986). 
Prices (Metz) contains grain prices supplied by Rainer Metz and compiled for the printed edition of Dietrich Ebeling and Franz Irsigler, Getreideumsatz, Getreide- und Brotpreise in Köln, 1368-1797 (Köln, 1976). 
Prices (Posthumus) contains prices drawn from primary sources and published in Nicholaas Wilhelmus Posthumus, Nederlandsche Prijsgeschiedenis (Leiden, 1943).

### Lund historical database
- Link: http://www.ekh.lu.se/en/research/economic-history-data/lu-madd
- Description: The Lund University Macroeconomic and Demographic Database (LU - MADD) is developed and maintained by the Department of Economic History at Lund University. The goal of the database is to assemble in one place a large and detailed collection of annual demographic and macroeconomic data about Sweden, and to make these data easily accessible to students, teachers and researchers in Sweden and around the world.

### Piketty’s data
- Link: http://piketty.pse.ens.fr/fr/publications
- Description: The data from the publications of Piketty, mostly about inequalities, return on capital, etc.

### World wealth and income database
- Link: http://www.wid.world
- Description: The World Wealth and Income Database (WID) aims to provide convenient open access to the most extensive available data series on the world distribution of income and wealth. Thanks to the cumulative effort of an international network of over ninety researchers covering nearly seventy countries, we have constructed during the past fifteen years the largest existing historical database on the long-run dynamics of income and wealth inequality. Some of these series, particularly those on top income shares, have already had a large impact on the global inequality debate.

### University of Texas Inequality Project
- Link: http://utip.lbj.utexas.edu
- Description: Data sets on pay inequality at the global level, at the national level including for Argentina, Brazil, Cuba, China, India, and Russia, and at the regional level for Europe. Pay inequality as an instrument to estimate measures of household income inequality, for a large panel of countries from 1963 through 1999. This new global data set has nearly 3,200 country-year observations. All data sets are available in the data section.

### Trans-Atlantic Slave Trade Database
- Link: http://slavevoyages.org
- Description: The Trans-Atlantic Slave Trade Database has information on almost 36,000 slaving voyages that forcibly embarked over 10 million Africans for transport to the Americas between the sixteenth and nineteenth centuries. The actual number is estimated to have been as high as 12.5 million. The database and the separate estimates interface offer researchers, students and the general public a chance to rediscover the reality of one of the largest forced movements of peoples in world history.

### Roman Empire geographical database
- Link: http://pelagios.org/maps/greco-roman
- Description: Detailed geographical information about the geography of the Roman Empire.

### Acemoglu’s Database
- Link: http://economics.mit.edu/faculty/acemoglu/data/aj2005
- Description: Database of the paper “Unbundling institutions”, on property rights, GDP, investment, credit, stock markets, for a wide range of countries and dates.

### Long Term Productivity Database
- Link: http://www.longtermproductivity.com
- Description: The database gathers series on total factor productivity and labour productivity per hours worked, capital intensity, and GDP per capita, for 17 OECD countries.
