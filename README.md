**1) [Data archiving and diffusion](Data\ access\ and\ curation.md) (by Bruno)**

- Data repositories
- New tools of data diffusion (e.g. data papers)
- The process to reproductible research

**2) [Open Data and Openly Accessible Data](Open\ Data\ and\ Open\ Access\ Data.md) (by Nada)**

- Government data
- Non governmental organizations data
- Linked open data (LOD) and linked open vocabulary (LOV)

**3) Traditional datasets**

- a) [Interesting Data Sources for Social Sciences](Interesting\ Data\ Sources.md) (by Timothy)

- b) [Historical Data Sources](Historical\ Data.md) (by Julien)

**4) Tools and algorithms to process data**

- a) [Data Extraction and Cleaning](Data\ Extraction\ and\ Cleaning.md) (by Sid and Nada)
    - Data cleaning  
	- Data extraction from the web (if possible use APIs)
	- Analyse textual data
	- Use of semantic technologies to annotate and search data

- b) [Data Classification and analysis](data\ classsification\ and\ analysis.md) (by Sid)
	- Classification
    - Clustering
    - Forecasting
    - Regression analysis
    - Knowledge discovery

**5) Statistical (Econometric) Techniques for Research in Social Sciences (by Timothy - and Aline if she can)**

- [A general introduction](introeconometrics.md) (by Timothy)
- [Resources for Econometrics](resourceseconometrics.md) (by Timothy)