***A Brief Introduction of Econometrics***

As Ragnar Frisch stressed in the Editor's Note of the very first issue of Econometrica in 1933, Econometrics is not Statistics because practitioners should consider both economic and statistical models at once. Econometrics has been developed to expand the range of usefulness of Statistics and also to deepen the understanding of the estimates. 

Economics is a discipline dealing with causality in addition to correlation in the economy. Traditional statistics is however not concerned with causality. This subtle difference separates Statistics and Econometrics. Most of the social scientists are aware of multi-variable regression, and they often jump to conclusion carelessly. Correlation does not imply causality. Sometimes apparent correlation is only a result of misspecification of the model employed.  

**Endogeneity Bias**

An often-found mistake is the missing of common factors. For example, we find strong positive correlation between sales amount and number of employees. It may simply because the economy is expanding, but there is no causality between number of employees and sales amount.

Another mistake is reverse causality. We find strong correlation between democracy and income level. But which is the cause? One can easily check with multivariable regression that no matter which is the dependent variable the strong correlation persists.

Econometrics group all these related problems as "endogeneity bias". To deal with the bias, economists have developed a tool called Instrumental-Variable estimation (IV estimation or 2-stage least-squared estimation). The idea is that, even if an explanatory variable, x, is endogenous determined, as long as we can find an exogenous variable, z, which is (sufficiently) correlated with x but does not theoretically explain the dependent variable, y, we can identify the effect of x on y. In other words, we regress y on the part of x which is explained by z so as to correct the endogeneity bias. Readers may read Wooldridge (2015) for a more detailed explanation.


*Wooldridge, Jeffrey M. Introductory econometrics: A modern approach. Nelson Education, 2015*
