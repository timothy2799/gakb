# Similar initiatives 

Here after a list of initiatives similar to Governance Analytics (the list is far from being exhaustive) : 

* [University of Virginia Library,  Research Data Services + Sciences](http://data.library.virginia.edu/datasources/) : At Research Data Services, researchers across disciplines benefit from expert consultation and training in acquiring, collecting, wrangling, analyzing, visualizing, sharing, and preserving research data. 
* [Harvard Library, The Dataverse Project](http://library.harvard.edu/gdc) : Dataverse is an open source web application to share, preserve, cite, explore, and analyze research data. It facilitates making data available to others, and allows you to replicate others' work more easily.
* [Plateform ISC-PIF](https://iscpif.fr/itservices/) :
    1.  Infrastructure and Platform as a service : [*itservices*](https://iscpif.fr/itservices/)
    2.  Open platforms : [*open-platforms*](https://iscpif.fr/services/open-platforms/)
